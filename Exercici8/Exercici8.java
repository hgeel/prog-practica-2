package Exercici8;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici8 {

	public static void main(String[] args) {
		
		while(true){

			System.out.println("*** EUROCONVERSOR ***");

			System.out.println("\nConvertir a: ");
			System.out.println("\t1. Pessetes.");
			System.out.println("\t2. Euros.");
			System.out.println("\t3. Sortir.");
			System.out.print("> ");
			int op = Keyboard.readInt();
			
			if(op == 3) {
				break;
			}
			
			System.out.println("\nEscriu la quantitat: ");
			System.out.print("> ");
			double val = Keyboard.readDouble();

			if (op == 1) {
				val *= 166.386;
				System.out.println(val + " pessetes.");
			} else if (op == 2) {
				val /= 166.386;
				System.out.println(val + " euros.");
			} else {
				System.out.println("No existeix la opció " + op);
			}
			
			System.out.println("\n");

		}
		
		System.out.println("Adeu.");

	}

}
