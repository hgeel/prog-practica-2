package Exercici2;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */

public class Exercici2 {

	public static void main(String[] args) {
		
		int a, b, c;
		
		System.out.println("*** Ordenar 3 nombres enters.");
		System.out.print("Primer nombre: ");
		a = Keyboard.readInt();
		System.out.print("Segon nombre: ");
		b = Keyboard.readInt();
		System.out.print("Tercer nombre: ");
		c = Keyboard.readInt();
		
		if(a > b) {
			int i = a;
			a = b;
			b = i;
		}
		
		if(b > c) {
			int i = b;
			b = c;
			c = i;
		}
		
		if(a > b) {
			int i = a;
			a = b;
			b = i;
		}
		
		System.out.println("En ordre: " + a + " " + b + " " + c);
		
	}
	
}
