package Exercici6;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici6 {

	public static void main(String[] args) {
		
		double maxh = 2.5, minh = 0.4;
		double maxw = 234, minw = 3;
		
		System.out.println("*** Calculadora IMC ***");
		
		System.out.print("Pes generat aleatoriament (en Kg): ");
		double w = Math.random() * (maxw - minw) + minw;
		w = Math.rint(w * 100) / 100;
		System.out.println(w);
		
		System.out.print("Al�ada generada aleatoriament (en m): ");
		double h = Math.random() * (maxh - minh) + minh;
		h = Math.rint(h * 100) / 100;
		System.out.println(h);
		
		System.out.println("------------");
		
		double imc = w / Math.pow(h, 2);
		imc = Math.rint(w * 100) / 100;
		System.out.print("El imc �s " + imc);
		
		if(imc < 18) {
			System.out.print(" considerat infrap�s");
			if(imc >= 17) {
				System.out.println(" moderat.");
			} else if(imc < 17 && imc >= 16) {
				System.out.println(" considerable.");
			} else {
				System.out.println(" sever.");
			}
		} else if(imc >= 18 && imc <= 25) {
			System.out.println(" considerat normal.");
		} else {
			System.out.print(" considerat obesitat");
			if(imc > 25 && imc <= 30) {
				System.out.println(" lleu.");
			} else if(imc > 30 && imc <= 35) {
				System.out.println(" moderada.");
			} else if(imc > 35 && imc <= 40) {
				System.out.println(" prem�rbida.");
			} else {
				System.out.println(" m�rbida.");
			}
		}
		
	}
	
}
