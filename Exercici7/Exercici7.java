package Exercici7;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici7 {

	public static void main(String[] args) {
		
		System.out.println("*** ARRELS D'UNA EQUACIO ***");
		System.out.println("Sent una equaci� de segon grau \"ax^2 + bx + c = 0\"");
		System.out.print("Escriu a: ");
		double a = Keyboard.readDouble();
		System.out.print("Escriu b: ");
		double b = Keyboard.readDouble();
		System.out.print("Escriu c: ");
		double c = Keyboard.readDouble();
		
		System.out.println();
		
		double insqrt = b*b - 4*a*c;
		if(insqrt < 0) {
			System.out.println("No te solucions reals.");
		} else if(insqrt == 0) {
			System.out.println("Una soluci�: ");
			System.out.println("Soluci�: " + (-b / (2*a)));
		} else {
			System.out.println("Dues solucions: ");
			System.out.println("Soluci� 1: " + (-b + Math.sqrt(insqrt)) / (2*a));
			System.out.println("Soluci� 1: " + (-b - Math.sqrt(insqrt)) / (2*a));
		}
		
	}
	
}
