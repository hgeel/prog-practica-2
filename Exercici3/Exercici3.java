package Exercici3;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */

public class Exercici3 {

	public static void main(String[] args) {
		
		System.out.println("*** EUROCONVERSOR ***");
		
		System.out.println("Escriu la quantitat: ");
		System.out.print("> ");
		double val = Keyboard.readDouble();
		System.out.println("Convertir a: ");
		System.out.println("\t1. Pessetes.");
		System.out.println("\t2. Euros.");
		System.out.print("> ");
		int op = Keyboard.readInt();
		
		if(op == 1) {
			val *= 166.386;
			System.out.println(val + " pessetes.");
		} else if(op == 2) {
			val /= 166.386;
			System.out.println(val + " euros.");
		} else {
			System.out.println("No existeix la opci� " + op);
		}
		
	}
	
}
