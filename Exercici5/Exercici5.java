package Exercici5;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici5 {

	public static void main(String[] args) {
		
		System.out.println("*** Calculadora IMC ***");
		System.out.print("Escriu el pes (en Kg): ");
		double w = Keyboard.readDouble();
		System.out.print("Escriu l'al�ada (en m): ");
		double h = Keyboard.readDouble();
		
		System.out.println("------------");
		
		double imc = w / Math.pow(h, 2);
		System.out.print("El imc �s " + imc);
		
		if(imc < 18) {
			System.out.print(" considerat infrap�s");
			if(imc >= 17) {
				System.out.println(" moderat.");
			} else if(imc < 17 && imc >= 16) {
				System.out.println(" considerable.");
			} else {
				System.out.println(" sever.");
			}
		} else if(imc >= 18 && imc <= 25) {
			System.out.println(" considerat normal.");
		} else {
			System.out.print(" considerat obesitat");
			if(imc > 25 && imc <= 30) {
				System.out.println(" lleu.");
			} else if(imc > 30 && imc <= 35) {
				System.out.println(" moderada.");
			} else if(imc > 35 && imc <= 40) {
				System.out.println(" prem�rbida.");
			} else {
				System.out.println(" m�rbida.");
			}
		}
		
	}
	
}
