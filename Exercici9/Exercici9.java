package Exercici9;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici9 {
	
	public static void main(String[] args) {
		
		System.out.println("Escriu totes les lletres de la frase una a una i un punt per acabar.");
		
		char c = ' ';
		int countA = 0;
		int countE = 0;
		int countI = 0;
		int countO = 0;
		int countU = 0;
		
		while(c != '.') {
			
			c = Keyboard.readChar();
			
			if(c == 'A') {
				countA++;
			} else if(c == 'E') {
				countE++;
			} else if(c == 'I') {
				countI++;
			} else if(c == 'O') {
				countO++;
			} else if(c == 'U') {
				countU++;
			}
			
		}
		
		System.out.println("Comptador de vocals majúscules:");
		System.out.println("Nombre de A: " + countA);
		System.out.println("Nombre de E: " + countE);
		System.out.println("Nombre de I: " + countI);
		System.out.println("Nombre de O: " + countO);
		System.out.println("Nombre de U: " + countU);
		
	}

}
