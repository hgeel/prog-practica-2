package Exercici10;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici10 {

	public static void main(String[] args) {

		System.out.println("Escriu totes les lletres de la frase una a una i un punt per acabar.");

		char c = ' ';
		int countA = 0;
		int countE = 0;
		int countI = 0;
		int countO = 0;
		int countU = 0;
		
		int countVoc = 0;
		int countCons = 0;

		while (c != '.') {

			c = Keyboard.readChar();

			//Per comptar vocals maj�scules.
			if (c == 'A') {
				countA++;
			} else if (c == 'E') {
				countE++;
			} else if (c == 'I') {
				countI++;
			} else if (c == 'O') {
				countO++;
			} else if (c == 'U') {
				countU++;
			}
			
			//Pel percentatge de vocals i consonants.
			if(c == 'a' || c == 'A') {
				countVoc++;
			} else if(c == 'e' || c == 'E') {
				countVoc++;
			} else if(c == 'i' || c == 'I') {
				countVoc++;
			} else if(c == 'o' || c == 'O') {
				countVoc++;
			} else if(c == 'u' || c == 'U') {
				countVoc++;
			} else if(c == '.') {
				//No es compta perque �s punt.
			} else {
				countCons++;
			}
			
		}

		//Imprimir nombre de aparicions de cada vocal.
		System.out.println("Comptador de vocals maj�scules:");
		System.out.println("Nombre de A: " + countA);
		System.out.println("Nombre de E: " + countE);
		System.out.println("Nombre de I: " + countI);
		System.out.println("Nombre de O: " + countO);
		System.out.println("Nombre de U: " + countU);
		System.out.println();
		
		//Calcular percentatge
		int total = countVoc + countCons;
		int percVoc = (int) ((double) countCons / total * 100);
		int percCons = 100 - percVoc;
		
		System.out.println("Vocals: " + percVoc + " %");
		System.out.println("Consonants: " + percCons + " %");

	}

}
