package Exercici4;

import Keyboard.Keyboard;

/**
 * 
 * @author hgeel
 *
 */
public class Exercici4 {

	public static void main(String[] args) {
		
		System.out.println("*** Calculadora IMC ***");
		System.out.print("Escriu el pes (en Kg): ");
		double w = Keyboard.readDouble();
		System.out.print("Escriu l'al�ada (en m): ");
		double h = Keyboard.readDouble();
		
		System.out.println("------------");
		
		double imc = w / Math.pow(h, 2);
		System.out.print("El imc �s " + imc);
		
		if(imc < 18) {
			System.out.println(" considerat infrap�s.");
		} else if(imc >= 18 && imc <= 25) {
			System.out.println(" considerat normal.");
		} else {
			System.out.println(" considerat sobrep�s.");
		}
		
	}
	
}
